package com.kicklighterdesignstudio.mycarpal;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class AddVehicleActivity extends Activity {

	private MyCarPalApplication app;

	private ActionBar actionBar;
	private EditText vehicleName;
	private EditText vehicleYear;
	private EditText vehicleMake;
	private EditText vehicleModel;
	private EditText vehicleVin;
	private EditText vehicleLicense;
	private EditText vehicleMileageInitial;
	private EditText vehicleComments;

	private String vehicleNameText;
	private String vehicleYearText;
	private String vehicleMakeText;
	private String vehicleModelText;
	private String vehicleVinText;
	private String vehicleLicenseText;
	private String vehicleMileageInitialText;
	private String vehicleCommentsText;

	private int vehicleYearInt;
	private int vehicleMileageInitialInt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_vehicle);

		setupViews();

		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_add_vehicle, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.doneButton: {
			saveVehicle();
			return true;
		}
		}

		return true;
	}

	private void saveVehicle() {
		readInput();
		parseData();
		if (checkRequiredFields()) {
			DebugLogger.log("ADD_VEHICLE_ACTIVITY", "STORING VEHICLE: \"" + vehicleNameText + " - "
					+ vehicleYearInt + " " + vehicleMakeText + vehicleModelText + " - Vin: "
					+ vehicleVinText + " - License: " + vehicleLicenseText + " - Mileage: "
					+ vehicleMileageInitialInt + " Comments: " + vehicleCommentsText + "\"");

			Vehicle vehicleToStore = new Vehicle(vehicleNameText, vehicleVinText, vehicleMakeText,
					vehicleModelText, vehicleYearInt, vehicleLicenseText, vehicleMileageInitialInt,
					"", vehicleCommentsText);

			if (app.addVehicle(vehicleToStore))
				Toast.makeText(getApplicationContext(), "Your vehicle has been added!",
						Toast.LENGTH_SHORT).show();
			else
				Toast.makeText(getApplicationContext(),
						"There was an error adding your vehicle...", Toast.LENGTH_SHORT).show();

			finish();
		}
	}

	private void readInput() {
		vehicleNameText = vehicleName.getText().toString().trim();
		vehicleYearText = vehicleYear.getText().toString().trim();
		vehicleMakeText = vehicleMake.getText().toString().trim();
		vehicleModelText = vehicleModel.getText().toString().trim();
		vehicleVinText = vehicleVin.getText().toString().trim();
		vehicleLicenseText = vehicleLicense.getText().toString().trim();
		vehicleMileageInitialText = vehicleMileageInitial.getText().toString().trim();
		vehicleCommentsText = vehicleComments.getText().toString().trim();
	}

	private boolean checkRequiredFields() {
		return checkVehicleName() && checkVehicleVin();
	}

	private boolean checkVehicleName() {
		if (vehicleNameText.equals("") || vehicleNameText.equals(null)) {
			vehicleName.requestFocus();
			Toast.makeText(getApplicationContext(), "Please name your vehicle.", Toast.LENGTH_LONG)
					.show();
			return false;
		}
		return true;
	}

	private boolean checkVehicleVin() {
		if (!(vehicleVinText.equals(""))) {
			if (!(vehicleVinText.length() == 10 || vehicleVinText.length() == 13 || vehicleVinText
					.length() == 17)) {
				vehicleVin.requestFocus();
				Toast.makeText(getApplicationContext(), "Please enter a valid VIN.",
						Toast.LENGTH_LONG).show();
				return false;
			}
		}
		return true;
	}

	private void parseData() {
		parseVehicleYear();
		parseVehicleMileageInitial();
	}

	private void parseVehicleYear() {
		if (!(vehicleYearText.equals(""))) {
			vehicleYearInt = Integer.parseInt(vehicleYearText);
		} else {
			vehicleYearInt = 0;
		}
	}

	private void parseVehicleMileageInitial() {
		if (!(vehicleMileageInitialText.equals(""))) {
			vehicleMileageInitialInt = Integer.parseInt(vehicleMileageInitialText);
		} else {
			vehicleYearInt = 0;
		}
	}

	private void setupViews() {
		app = (MyCarPalApplication) getApplication();
		actionBar = getActionBar();
		vehicleName = (EditText) findViewById(R.id.vehicleName);
		vehicleYear = (EditText) findViewById(R.id.vehicleYear);
		vehicleMake = (EditText) findViewById(R.id.vehicleMake);
		vehicleModel = (EditText) findViewById(R.id.vehicleModel);
		vehicleVin = (EditText) findViewById(R.id.vehicleVin);
		vehicleLicense = (EditText) findViewById(R.id.vehicleLicense);
		vehicleMileageInitial = (EditText) findViewById(R.id.vehicleMileageInitial);
		vehicleComments = (EditText) findViewById(R.id.vehicleComments);
	}

}
