package com.kicklighterdesignstudio.mycarpal;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CarPalDatabaseHelper extends SQLiteOpenHelper {

	public static final String DB_NAME = "MainRecords.sqlite";
	public static final int DB_VERSION = 1;

	public static final String VEHICLE_TABLE = "vehicles";
	public static final String VEHICLE_ID = "id";
	public static final String VEHICLE_NAME = "name";
	public static final String VEHICLE_VIN = "vin";
	public static final String VEHICLE_MAKE = "make";
	public static final String VEHICLE_MODEL = "model";
	public static final String VEHICLE_YEAR = "year";
	public static final String VEHICLE_LICENSE = "license";
	public static final String VEHICLE_MILIEAGE_INITIAL = "mileage_initial";
	public static final String VEHICLE_IMAGE_FILENAME = "image_filename";
	public static final String VEHICLE_COMMENTS = "comments";

	public static final String SERVICE_TABLE = "service";
	public static final String SERVICE_ID = "id";
	public static final String SERVICE_DATE = "date";
	public static final String SERVICE_VEHICLE_ID = "vehicle_id";
	public static final String SERVICE_MILEAGE = "mileage";
	public static final String SERVICE_RECORD_TABLE_ID = "record_table_id";
	public static final String SERVICE_COMMENTS = "comments";

	public static final String RECORD_TABLE = "records";
	public static final String RECORD_ID = "id";
	public static final String RECORD_NAME = "name";

	public static final String RECORD_USER_TABLE = "records_user";
	public static final String RECORD_USER_ID = "id";
	public static final String RECORD_USER_NAME = "name";

	public CarPalDatabaseHelper(Context context, String name) {
		super(context, name, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		String createVehicleTable = String
				.format("create table %s (%s integer unique primary key autoincrement not null, %s text unique, %s text, %s text, %s text, %s integer, %s text, %s integer, %s text, %s text)",
						VEHICLE_TABLE, VEHICLE_ID, VEHICLE_NAME, VEHICLE_VIN, VEHICLE_MAKE,
						VEHICLE_MODEL, VEHICLE_YEAR, VEHICLE_LICENSE, VEHICLE_MILIEAGE_INITIAL,
						VEHICLE_IMAGE_FILENAME, VEHICLE_COMMENTS);

		String createServiceTable = String
				.format("create table %s (%s integer unique primary key autoincrement not null, %s integer, %s integer, %s integer, %s text, %s text)",
						SERVICE_TABLE, SERVICE_ID, SERVICE_DATE, SERVICE_VEHICLE_ID,
						SERVICE_MILEAGE, SERVICE_RECORD_TABLE_ID, SERVICE_COMMENTS);

		String createRecordsTable = String.format(
				"create table %s (%s integer unique primary key autoincrement not null, %s text)",
				RECORD_TABLE, RECORD_ID, RECORD_NAME);

		String insertRecords = String.format("insert into %s (%s) values ('Oil Change')",
				RECORD_TABLE, RECORD_NAME);

		String insertFakeData = String.format(
				"insert into %s (%s, %s, %s, %s, %s) values (1363040971, 1, 99750, '11', '')",
				SERVICE_TABLE, SERVICE_DATE, SERVICE_VEHICLE_ID, SERVICE_MILEAGE,
				SERVICE_RECORD_TABLE_ID, SERVICE_COMMENTS);
		
		String insertFakeData2 = String.format(
				"insert into %s (%s, %s, %s, %s, %s) values (1363040975, 1, 99892, '11', '')",
				SERVICE_TABLE, SERVICE_DATE, SERVICE_VEHICLE_ID, SERVICE_MILEAGE,
				SERVICE_RECORD_TABLE_ID, SERVICE_COMMENTS);

		DebugLogger.log("DATABASE", "Create vehicle table: " + createVehicleTable);
		db.execSQL(createVehicleTable);

		DebugLogger.log("DATABASE", "Create service table: " + createServiceTable);
		db.execSQL(createServiceTable);

		DebugLogger.log("DATABASE", "Create records table: " + createRecordsTable);
		db.execSQL(createRecordsTable);

		DebugLogger.log("DATABASE", "Insert default records into table: " + insertRecords);
		db.execSQL(insertRecords);
		
		DebugLogger.log("DATABASE", "Insert fake data into table: " + insertFakeData);
		db.execSQL(insertFakeData);
		
		DebugLogger.log("DATABASE", "Insert fake data into table: " + insertFakeData2);
		db.execSQL(insertFakeData2);

		DebugLogger.log("DATABASE", "Tables created successfully.");

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

}
