package com.kicklighterdesignstudio.mycarpal;

import android.util.Log;

public class DebugLogger {

	public static void log(String logTitle, String logData) {
		Log.d(logTitle,logData);
	}
	
}
