package com.kicklighterdesignstudio.mycarpal;

import static com.kicklighterdesignstudio.mycarpal.CarPalDatabaseHelper.*;

import java.util.ArrayList;
import java.util.Collections;

import android.app.Application;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class MyCarPalApplication extends Application {

	private SQLiteDatabase mainDatabase;
	private ArrayList<Vehicle> vehicleList;
	private ArrayList<String> vehicleNameList;

	@Override
	public void onCreate() {
		super.onCreate();
		CarPalDatabaseHelper dbHelper = new CarPalDatabaseHelper(this, DB_NAME);
		mainDatabase = dbHelper.getWritableDatabase();
		readFromDatabase();
		setupVars();
	}

	private void readFromDatabase() {
		readVehiclesTable();
	}

	private void updateVehicleNameList() {
		Collections.sort(vehicleList);
		vehicleNameList.clear();
		for (Vehicle vehicle : vehicleList) {
			vehicleNameList.add(vehicle.getName());
		}
	}

	private void readVehiclesTable() {
		vehicleList = new ArrayList<Vehicle>();

		Cursor vehicleCursor;

		String[] columns = new String[] { VEHICLE_ID, VEHICLE_NAME, VEHICLE_VIN, VEHICLE_MAKE,
				VEHICLE_MODEL, VEHICLE_YEAR, VEHICLE_LICENSE, VEHICLE_MILIEAGE_INITIAL,
				VEHICLE_IMAGE_FILENAME, VEHICLE_COMMENTS };

		vehicleCursor = mainDatabase.query(VEHICLE_TABLE, columns, null, null, null, null,
				VEHICLE_ID);

		vehicleCursor.moveToFirst();
		Vehicle tempVehicle;

		if (!vehicleCursor.isAfterLast()) {
			do {
				long id = vehicleCursor.getLong(0);
				String name = vehicleCursor.getString(1);
				String vin = vehicleCursor.getString(2);
				String make = vehicleCursor.getString(3);
				String model = vehicleCursor.getString(4);
				int year = vehicleCursor.getInt(5);
				String license = vehicleCursor.getString(6);
				long mileageInitial = vehicleCursor.getLong(7);
				String imageFilename = vehicleCursor.getString(8);
				String comments = vehicleCursor.getString(9);

				tempVehicle = new Vehicle(id, name, vin, make, model, year, license,
						mileageInitial, imageFilename, comments);

				vehicleList.add(tempVehicle);

			} while (vehicleCursor.moveToNext());
		}

	}

	public boolean addVehicle(Vehicle vehicleToAdd) {
		ContentValues newVehicle = new ContentValues();
		newVehicle.put(VEHICLE_NAME, vehicleToAdd.getName());
		newVehicle.put(VEHICLE_VIN, vehicleToAdd.getVin());
		newVehicle.put(VEHICLE_MAKE, vehicleToAdd.getMake());
		newVehicle.put(VEHICLE_MODEL, vehicleToAdd.getModel());
		newVehicle.put(VEHICLE_YEAR, vehicleToAdd.getYear());
		newVehicle.put(VEHICLE_LICENSE, vehicleToAdd.getLicense());
		newVehicle.put(VEHICLE_MILIEAGE_INITIAL, vehicleToAdd.getMileageInitial());
		newVehicle.put(VEHICLE_IMAGE_FILENAME, vehicleToAdd.getImageFilename());
		newVehicle.put(VEHICLE_COMMENTS, vehicleToAdd.getComments());

		DebugLogger.log("APPLICATION", "Inserting data into DB: " + vehicleToAdd);

		long idPassedBack = mainDatabase.insert(VEHICLE_TABLE, null, newVehicle);

		DebugLogger.log("APPLICATION", "ID = " + idPassedBack);

		if (idPassedBack >= 0) {
			vehicleToAdd.setId(idPassedBack);
			vehicleList.add(vehicleToAdd);
			updateVehicleNameList();
			DebugLogger.log("APPLICATION", "Vehicle added to Array List.");
			return true;
		}

		return false;
	}

	public boolean removeVehicle(Vehicle vehicleToDestroy) {
		if (!(vehicleList.contains(vehicleToDestroy)))
			return false;
		int vehicleIndex = vehicleList.indexOf(vehicleToDestroy);
		long vehicleId = vehicleList.get(vehicleIndex).getId();
		int count = mainDatabase.delete(VEHICLE_TABLE, VEHICLE_ID + "=" + vehicleId, null);
		if (count > 0) {
			vehicleList.remove(vehicleToDestroy);
			updateVehicleNameList();
			return true;
		}
		return false;
	}

	public boolean addServiceRecord(ServiceRecord serviceRecordToStore) {
		DebugLogger.log("APPLICATION", "Adding service record to DB: " + serviceRecordToStore);
		ContentValues newServiceRecord = new ContentValues();
		newServiceRecord.put(SERVICE_DATE, serviceRecordToStore.getDate());
		newServiceRecord.put(SERVICE_VEHICLE_ID, serviceRecordToStore.getVehicleId());
		newServiceRecord.put(SERVICE_MILEAGE, serviceRecordToStore.getMileage());
		newServiceRecord.put(SERVICE_RECORD_TABLE_ID, serviceRecordToStore.getRecordTableIdString());
		newServiceRecord.put(SERVICE_COMMENTS, serviceRecordToStore.getComments());

		long idPassedBack = mainDatabase.insert(SERVICE_TABLE, null, newServiceRecord);
		DebugLogger.log("APPLICATION", "Added service record with id: " + idPassedBack);

		return false;
	}

	public void generateVehicles() {
		Vehicle newVehicle = new Vehicle("Jordan's Xterra", "AOEUEOUEOE", "Nissan", "Xterra", 2006,
				"EUEUEU", 99150, "", "This is my totally awesome Xterra.");
		addVehicle(newVehicle);
		newVehicle = new Vehicle("Cameron's Taurus", "2356482045", "Ford", "Taurus", 1990,
				"EUEUEU", 265489, "", "This is my roomate's Ford Taurus.");
		addVehicle(newVehicle);
	}

	public void generateServiceRecords() {
		ServiceRecord store = new ServiceRecord(1363040971, 1, 99500, "11", "First oil change.");
		addServiceRecord(store);
		store = new ServiceRecord(1363040975, 1, 99784, "11", "Second oil change.");
		addServiceRecord(store);
	}

	public ArrayList<ServiceRecord> getRecordsForVehicle(long vehicleIdToGet) {
		ArrayList<ServiceRecord> al = new ArrayList<ServiceRecord>();

		Cursor recordsCursor;

		String[] columns = new String[] { SERVICE_ID, SERVICE_DATE, SERVICE_VEHICLE_ID,
				SERVICE_MILEAGE, SERVICE_RECORD_TABLE_ID, SERVICE_COMMENTS };

		String selection = SERVICE_VEHICLE_ID + " = " + vehicleIdToGet;

		recordsCursor = mainDatabase.query(SERVICE_TABLE, columns, selection, null, null, null,
				SERVICE_DATE);

		recordsCursor.moveToFirst();

		if (!recordsCursor.isAfterLast()) {
			do {
				long id = recordsCursor.getLong(0);
				long date = recordsCursor.getLong(1);
				long vehicleId = recordsCursor.getLong(2);
				int mileage = recordsCursor.getInt(3);
				String recordTableIdString = recordsCursor.getString(4);
				String comments = recordsCursor.getString(5);

				ServiceRecord tempRecord = new ServiceRecord(id, date, vehicleId, mileage,
						recordTableIdString, comments);

				al.add(tempRecord);

			} while (recordsCursor.moveToNext());
		}

		return al;
	}

	public String getRecordName(int tableId, int recordId) {
		DebugLogger.log("APPLICATION", "Retrieving record from table: " + tableId + " with id: " + recordId);
		String recordName;
		String tableName;
		String[] columns = new String[] { RECORD_NAME };
		String selection = RECORD_ID + " = " + recordId;

		switch (tableId) {
		case 2: {
			tableName = RECORD_USER_TABLE;
			break;
		}
		default: {
			tableName = RECORD_TABLE;
			break;
		}
		}

		Cursor recordCursor = mainDatabase.query(tableName, columns, selection, null, null, null,
				null);
		recordCursor.moveToFirst();
		recordName = recordCursor.getString(0);
		
//		Cursor recordCursor = mainDatabase.rawQuery("SELECT * FROM records", null);
		
		DebugLogger.log("APPLICATION", "Number of records found: " + recordCursor.getCount());
		

		return recordName;
	}

	public SQLiteDatabase getMainDatabase() {
		return mainDatabase;
	}

	public ArrayList<String> getVehicleNameList() {
		return vehicleNameList;
	}

	public void setVehicleNameList(ArrayList<String> vehicleNameList) {
		this.vehicleNameList = vehicleNameList;
	}

	public void setMainDatabase(SQLiteDatabase mainDatabase) {
		this.mainDatabase = mainDatabase;
	}

	public ArrayList<Vehicle> getVehicleList() {
		return vehicleList;
	}

	public void setVehicleList(ArrayList<Vehicle> vehicleList) {
		this.vehicleList = vehicleList;
	}

	private void setupVars() {
		vehicleNameList = new ArrayList<String>();
		updateVehicleNameList();
	}

}
