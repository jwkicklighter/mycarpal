package com.kicklighterdesignstudio.mycarpal;

import java.util.ArrayList;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ServiceListActivity extends Activity {

	private MyCarPalApplication app;
	private ArrayList<String> vehicleNameList;
	private ArrayList<Vehicle> vehicleList;
	private ActionBar actionBar;
	private Vehicle selectedVehicle;
	ArrayList<ServiceRecord> serviceRecordList;
	private SharedPreferences sharedPreferences;

	private ListView serviceListView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		DebugLogger.log("SERVICE_LIST_ACTIVITY", "OnCreate()");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_service_list);
		setupVars();
		setupActionBar();
		// app.generateVehicles();
		// app.generateServiceRecords();
	}

	@Override
	protected void onResume() {
		super.onResume();
		updateActionBarNavigation();
		checkNumberOfVehicles();
		loadNavigationSelection();
	}

	@Override
	protected void onPause() {
		super.onPause();
		saveNavigationSelection();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_service_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.menu_add_vehicle: {
			Intent addVehicle = new Intent(this, AddVehicleActivity.class);
			startActivity(addVehicle);
			return super.onOptionsItemSelected(item);
		}
		case R.id.menu_delete_vehicle: {
			deleteVehicle();
			return super.onOptionsItemSelected(item);
		}
		case R.id.menu_settings: {
			Toast.makeText(getApplicationContext(), "Settings menu selected.", Toast.LENGTH_SHORT)
					.show();
			return super.onOptionsItemSelected(item);
		}
		}

		return super.onOptionsItemSelected(item);
	}

	private void setupActionBar() {
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		actionBar.setDisplayShowTitleEnabled(false);
	}

	private void updateActionBarNavigation() {

		vehicleList = app.getVehicleList();
		vehicleNameList = app.getVehicleNameList();

		ArrayAdapter<CharSequence> actionBarAdapter;
		actionBarAdapter = new ArrayAdapter<CharSequence>(this,
				android.R.layout.simple_spinner_item);
		actionBarAdapter.addAll(vehicleNameList);
		actionBarAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		actionBar.setListNavigationCallbacks(actionBarAdapter,
				new ActionBar.OnNavigationListener() {
					@Override
					public boolean onNavigationItemSelected(int itemPosition, long itemId) {
						DebugLogger.log("SERVICE_LIST_ACTIVITY", "Selected item: " + itemPosition);
						displayVehicleInfo(itemPosition);
						setSelectedVehicle(itemPosition);
						return false;
					}
				});
	}

	private void deleteVehicle() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Delete Vehicle?")
				.setMessage(
						"Are you sure you want to delete " + selectedVehicle.getName()
								+ "? This can't be undone...").setCancelable(true)
				.setPositiveButton("Yes, delete!", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (app.removeVehicle(ServiceListActivity.this.selectedVehicle)) {
							Toast.makeText(
									getApplicationContext(),
									"Successfully deleted vehicle \""
											+ ServiceListActivity.this.selectedVehicle.getName()
											+ "\"", Toast.LENGTH_SHORT).show();
							updateActionBarNavigation();
							actionBar.setSelectedNavigationItem(0);
						}
					}
				}).setNegativeButton("No, cancel.", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void checkNumberOfVehicles() {
		if (vehicleList.size() == 0)
			alertNoVehicles();
		else
			return;
	}

	private void alertNoVehicles() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("No Vehicles!")
				.setMessage(
						"You don't have any vehicles! Without a vehicle profile, you can't save any data. Would you like to create one now?")
				.setCancelable(true)
				.setPositiveButton("Yes, please!", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Intent addVehicle = new Intent(ServiceListActivity.this,
								AddVehicleActivity.class);
						startActivity(addVehicle);
					}
				}).setNegativeButton("No, thanks.", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				}).setOnCancelListener(new DialogInterface.OnCancelListener() {
					@Override
					public void onCancel(DialogInterface dialog) {
						finish();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void setupVars() {
		app = (MyCarPalApplication) getApplication();
		actionBar = getActionBar();
		serviceListView = (ListView) findViewById(R.id.serviceListView);
		sharedPreferences = getPreferences(MODE_PRIVATE);
	}

	private void setSelectedVehicle(int id) {
		selectedVehicle = vehicleList.get(id);
	}

	private void displayVehicleInfo(int idToDisplay) {
		Vehicle vehicle = vehicleList.get(idToDisplay);
		serviceRecordList = app.getRecordsForVehicle(vehicle.getId());
		ArrayAdapter<String> adapter;
		String stringToDisplay;

		if (serviceRecordList.size() > 0) {
			ArrayList<String> serviceRecordStringList = new ArrayList<String>();
			for (ServiceRecord serviceRecord : serviceRecordList) {
				stringToDisplay = serviceRecord.getDate()
						+ " - "
						+ app.getRecordName(serviceRecord.getRecordTable(),
								serviceRecord.getRecordId());
				serviceRecordStringList.add(stringToDisplay);
			}
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
					serviceRecordStringList);
		} else {
			adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
					new String[] { "There are no records to display." });
		}

		serviceListView.setAdapter(adapter);
	}

	private void saveNavigationSelection() {
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putLong("navigationSelection", actionBar.getSelectedNavigationIndex());
		editor.commit();
	}

	private void loadNavigationSelection() {
		actionBar.setSelectedNavigationItem((int) sharedPreferences.getLong("navigationSelection",
				0));
	}

}
