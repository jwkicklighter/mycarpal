package com.kicklighterdesignstudio.mycarpal;

import android.annotation.SuppressLint;

public class ServiceRecord implements Comparable<ServiceRecord> {

	private long id;
	private long date;
	private long vehicleId;
	private int mileage;
	private String recordTableIdString;
	private int recordTable;
	private int recordId;
	private String comments;

	public ServiceRecord(long id, long date, long vehicleId, int mileage,
			String recordTableIdString, String comments) {
		this.id = id;
		this.date = date;
		this.vehicleId = vehicleId;
		this.mileage = mileage;
		this.recordTableIdString = recordTableIdString;
		this.recordTable = Integer.parseInt(recordTableIdString.substring(0, 1));
		this.recordId = Integer.parseInt(recordTableIdString.substring(1));
		this.comments = comments;
	}
	
	public ServiceRecord(long date, long vehicleId, int mileage,
			String recordTableIdString, String comments) {
		this.date = date;
		this.vehicleId = vehicleId;
		this.mileage = mileage;
		this.recordTableIdString = recordTableIdString;
		this.recordTable = Integer.parseInt(recordTableIdString.substring(0, 1));
		this.recordId = Integer.parseInt(recordTableIdString.substring(1));
		this.comments = comments;
	}

	@Override
	public int compareTo(ServiceRecord other) {
		return Long.toString(date).compareTo(Long.toString(other.getDate()));
	}

	@SuppressLint("DefaultLocale")
	@Override
	public String toString() {
		return String.format(
				"Record %d - %d Vehicle: %d, mileage: %d, table+id: %s table: %d, id: %d, comments: %s", id,
				date, vehicleId, mileage, recordTableIdString, recordTable, recordId, comments);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public long getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(long vehicleId) {
		this.vehicleId = vehicleId;
	}

	public int getMileage() {
		return mileage;
	}

	public void setMileage(int mileage) {
		this.mileage = mileage;
	}

	public String getRecordTableIdString() {
		return recordTableIdString;
	}

	public void setRecordTableIdString(String recordTableIdString) {
		this.recordTableIdString = recordTableIdString;
	}

	public int getRecordTable() {
		return recordTable;
	}

	public void setRecordTable(int recordTable) {
		this.recordTable = recordTable;
	}

	public int getRecordId() {
		return recordId;
	}

	public void setRecordId(int recordId) {
		this.recordId = recordId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

}
