package com.kicklighterdesignstudio.mycarpal;

import android.annotation.SuppressLint;

public class Vehicle implements Comparable<Vehicle> {

	private long id;
	private String name;
	private String vin;
	private String make;
	private String model;
	private int year;
	private String license;
	private long mileageInitial;
	private String imageFilename;
	private String comments;

	public void vehicle(long id, String name) {
		this.id = id;
		this.name = name;
	}

	public Vehicle(long id, String name, String vin, String make, String model, int year,
			String license, long mileageInitial, String imageFilename, String comments) {
		this.id = id;
		this.name = name;
		this.vin = vin;
		this.make = make;
		this.model = model;
		this.year = year;
		this.license = license;
		this.mileageInitial = mileageInitial;
		this.imageFilename = imageFilename;
		this.comments = comments;
	}
	
	public Vehicle(String name, String vin, String make, String model, int year,
			String license, long mileageInitial, String imageFilename, String comments) {
		this.id = 0;
		this.name = name;
		this.vin = vin;
		this.make = make;
		this.model = model;
		this.year = year;
		this.license = license;
		this.mileageInitial = mileageInitial;
		this.imageFilename = imageFilename;
		this.comments = comments;
	}

	@SuppressLint("DefaultLocale")
	@Override
	public String toString() {
		return String.format("Name: %s, %d %s %s", name, year, make, model);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	public long getMileageInitial() {
		return mileageInitial;
	}

	public void setMileageInitial(long mileageInitial) {
		this.mileageInitial = mileageInitial;
	}

	public String getImageFilename() {
		return imageFilename;
	}

	public void setImageFilename(String imageFilename) {
		this.imageFilename = imageFilename;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
	public int compareTo(Vehicle other) {
		return getName().compareTo(other.getName());
	}

}
